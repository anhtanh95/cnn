package com.rezonia.cnn

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.FileUtils
import android.provider.MediaStore
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.*


class MainActivity : AppCompatActivity() {

    companion object {
        const val API_URL = "http://www.android.com/"
    }

    // General activity result contract
    private val openPickFile =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermission.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
                } else {
                    val uriExternal: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    val listOfAllImages: MutableList<Uri> = mutableListOf()
                    contentResolver.query(
                        uriExternal,
                        arrayOf(MediaStore.Images.Media._ID),
                        null,
                        null,
                        null
                    )?.let { cursor ->
                        val columnIndexID =
                            cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
                        var imageId: Long
                        while (cursor.moveToNext()) {
                            imageId = cursor.getLong(columnIndexID)
                            val uriImage = Uri.withAppendedPath(uriExternal, "" + imageId)
                            listOfAllImages.add(uriImage)
                        }
                        cursor.close()
                        if (listOfAllImages.size <= 0) return@let

                        val imageView: ImageView = findViewById(R.id.imageView2)
                        val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            val source = ImageDecoder.createSource(this.contentResolver, listOfAllImages[0])
                            ImageDecoder.decodeBitmap(source)
                        } else {
                            MediaStore.Images.Media.getBitmap(this.contentResolver, listOfAllImages[0])
                        }
                        imageView.setImageBitmap(bitmap)
                    }
                }
            }
        }

    // Request permission contract
    private val requestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            // Do something if permission granted
            if (isGranted) toast("Permission is granted")
            else toast("Permission is denied")
        }
    lateinit var imageView: ImageView
    lateinit var button: Button
    private lateinit var buttonUploadImage: Button
    private val pickImage = 100
    private var imageUri: Uri? = null
//    ==========================================================


//    ==========================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView = findViewById(R.id.imageView2)
        button = findViewById(R.id.buttonPickImage)
        button.setOnClickListener {
            val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            startActivityForResult(gallery, pickImage)
        }
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermission.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
        }

        buttonUploadImage = findViewById(R.id.buttonClassifyImage)
        buttonUploadImage.setOnClickListener {
            //uploadImage()
        }
    }

    private fun uploadImage(fileName: String, bmp: Bitmap) {
        val url = URL(API_URL)
        val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
        try {
            val boundary: String = UUID.randomUUID().toString()
            connection.requestMethod = "POST"
            connection.doOutput = true
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=$boundary")
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Cache-Control", "no-cache");

            val request = DataOutputStream(connection.outputStream)
            request.writeBytes("--$boundary\r\n")
            request.writeBytes("Content-Disposition: form-data; name=\"file\"; filename=\"$fileName\"\r\n\r\n")

            // Convert Bitmap to byteArray
            val stream = ByteArrayOutputStream()
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val byteArray: ByteArray = stream.toByteArray()
            bmp.recycle()

            request.write(byteArray)
            request.writeBytes("\r\n")

            request.writeBytes("--$boundary--\r\n")
            request.flush()

            when (connection.responseCode) {
                200 -> {

                }
            }
        } finally {
            connection.disconnect()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == pickImage) {
            imageUri = data?.data
            imageView.setImageURI(imageUri)
        }
    }

    private fun toast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }
}

